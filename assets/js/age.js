var jqxhr = $.getJSON( "http://mootoon.asuscomm.com:5000/age/1", function(data) {

  var age = _.chain(data) // enable chaining
  .values()            // object to array
  .flatten()           // 2D array to 1D array
  .pluck("age")       // pick one property from each element         
  .value();   
  
  $('.age').circleProgress({
    value: age[0]/98,
    size: 175,
    fill: {gradient: ["#003333", "#226666", "#407F7F", "#669999"]},
    animation:{ duration: 1500}
    }).on('circle-animation-progress', function(event, progress, stepValue) 
    {
        $(this).find('strong').text(age);
    });
});
