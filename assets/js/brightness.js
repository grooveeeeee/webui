var jqxhr = $.getJSON( "http://mootoon.asuscomm.com:5000/humid/1", function(data) {

  var humid = _.chain(data) // enable chaining
  .values()            // object to array
  .flatten()           // 2D array to 1D array
  .pluck("humidity")       // pick one property from each element         
  .value();   
  
  $('.brightness').circleProgress({
    value: humid[0]/100,
    size: 175,
    fill: {gradient: ["#EBDFD9", "#AA6239", "#FF965A", "#FF6D1B"]},
    animation:{ duration: 1500}
    }).on('circle-animation-progress', function(event, progress, stepValue) 
    {
        $(this).find('strong').text(humid);
    });

});
