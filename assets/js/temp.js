var time, targetT,internalT,externalT;
var ctx;
var myChart;

var jqxhr = $.getJSON( "http://mootoon.asuscomm.com:5000/temp/1");

var canvas = document.getElementById('tempChart'),
    ctx = canvas.getContext('2d');

var myLiveChart = new Chart(ctx, {
type: 'line',
data: {
    labels: [Date.now()],
    datasets: [{
        label: 'Internal',
        data: [25.6969589246437],
        borderWidth: 1,
        fill: false,
        backgroundColor: "rgb(13, 77, 77)",
        borderColor: "rgb(13, 77, 77)", 
        spanGaps: true
    },
    {
        label: 'External',
        data: [42.6969589246437],
        borderWidth: 1,
        fill: false,
        backgroundColor: "rgb(64,127,127)",
        borderColor: "rgb(64,127,127)", 
        spanGaps: true
    },
    {
        label: 'Target',
        data: [35.0],
        borderWidth: 4,
        fill: false,
        backgroundColor: "rgb(0, 51, 51)",
        borderColor: "rgb(0, 51, 51)",
        pointRadius:0,
        spanGaps: true
    }
    ]
},
options: {
    animation:false,
    elements: {
            // line: {
            //     tension: 0, // disables bezier curves
            // }
        },
        
    scales: {
        scaleLabel: {
            fontSize: 8
        }, 
    xAxes: [{
        ticks: {
                autoSkip: false,
                maxRotation: 45,
                minRotation: 45
            },
        type: 'time',
            time: {
                unit: "second",
                isoWeekday: false,
                drawTicks: false
            }
    }],
    yAxes: [{
        display: true,
        ticks: {
            min: 23,  
            max: 50,
            stepSize: 5
            }
        }]
    }
}
});

setInterval(function(){
    // Update one of the points in the second dataset

  myLiveChart.data.datasets[0].data.push(40.0*((Math.random() * (1.05 - 1.0)) + 1.0));
  myLiveChart.data.datasets[1].data.push(30.0*((Math.random() * (1.05 - 1.0)) + 1.0));
  myLiveChart.data.datasets[2].data.push(35.0);
  myLiveChart.data.labels.push(Date.now())

  if (myLiveChart.data.datasets[2].data.length >= 15){
    myLiveChart.data.datasets[0].data.shift();
    myLiveChart.data.datasets[1].data.shift();
    myLiveChart.data.datasets[2].data.shift();
    myLiveChart.data.labels.shift();
  }
  
  myLiveChart.update();
}, 3000);


// loadData(jqxhr);

// async function loadData(data) {
//     console.log("hola")
//      time = _.chain(data) // enable chaining
//   .values()            // object to array
//   .flatten()           // 2D array to 1D array
//   .pluck("ts")       // pick one property from each element         
//   .value();   

//    targetT = _.chain(data) // enable chaining
//   .values()            // object to array
//   .flatten()           // 2D array to 1D array
//   .pluck("targettemp")       // pick one property from each element         
//   .value(); 
 
//    internalT = _.chain(data) // enable chaining
//   .values()            // object to array
//   .flatten()           // 2D array to 1D array
//   .pluck("internaltemp")       // pick one property from each element         
//   .value(); 

//    externalT = _.chain(data) // enable chaining
//   .values()            // object to array
//   .flatten()           // 2D array to 1D array
//   .pluck("externaltemp")       // pick one property from each element         
//   .value();   

//     ctx = $("#tempChart").get(0).getContext('2d');
//     myChart = new Chart(ctx, {
//     type: 'line',
//     data: {
//         labels: time,
//         datasets: [{
//             label: 'Internal',
//             data: internalT,
//             borderWidth: 1,
//             fill: false,
//             backgroundColor: "rgb(13, 77, 77)",
//             borderColor: "rgb(13, 77, 77)", 
//         },
//         {
//             label: 'External',
//             data: externalT,
//             borderWidth: 1,
//             fill: false,
//             backgroundColor: "rgb(64,127,127)",
//             borderColor: "rgb(64,127,127)", 

//         },
//         {
//             label: 'Target',
//             data: targetT,
//             borderWidth: 4,
//             fill: false,
//             backgroundColor: "rgb(0, 51, 51)",
//             borderColor: "rgb(0, 51, 51)",
//             pointRadius:0
//         }
//         ]
//     },
//     options: {
//         scales: {
//         xAxes: [{
//             type: 'time',
//             time: {
//                 displayFormats: {
//                         quarter: 'h:mm:ss a'
//                     }
//                 }
//             }],
//         yAxes: [{
//             display: true,
//             ticks: {
//                 suggestedMin: 20,  
//                 max: 50,
//                 stepSize: 5
//                 }
//             }]
//         }
//     }

//     });
//   await sleep(2000);
// }







